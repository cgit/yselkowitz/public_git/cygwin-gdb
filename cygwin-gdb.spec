Name:           cygwin-gdb
Version:        7.10.1
Release:        1%{?dist}
Summary:        Cygwin GNU debugger

License:        GPLv3+
Group:          Development/Utilities
URL:            http://www.gnu.org/software/gdb/

Source0:        ftp://sourceware.org/pub/gdb/releases/gdb-%{version}.tar.xz
Patch0:         7.8-symtab-cygwin.patch
Patch1:         0001-Teach-gdb-how-to-unwind-cygwin-_sigbe-and-sigdelayed.patch
Patch2:         0002-7.8-windows-nat-cygwin.patch.patch
Patch3:         0003-Better-handling-for-realpath-failures-in-windows_mak.patch

BuildRequires:  expat-devel
BuildRequires:  ncurses-devel
%if 0%{!?rhel:1} || 0%{?rhel} > 7
BuildRequires:  python3-devel
%else
BuildRequires:  python-devel
%endif
%if 0%{!?rhel:1} || 0%{?rhel} > 6
BuildRequires:  xz-devel
%endif
BuildRequires:  zlib-devel
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  texinfo
BuildRequires:  cygwin32-filesystem
BuildRequires:  cygwin64-filesystem

%description
GNU debugger which understands Cygwin PE executables and DLLs,
using a gdbserver(1) instance on a Cygwin machine to run the executable.

%package -n cygwin32-gdb
Summary:        GNU Debugger for Cygwin 32bit toolchain
# NB: This must be left in.
Requires:       cygwin32-filesystem
Provides:       %{name} = %{version}-%{release}
Obsoletes:      %{name} < %{version}-%{release}

%description -n cygwin32-gdb
GNU debugger which understands Cygwin 32bit PE executables and DLLs,
using a gdbserver(1) instance on a Cygwin machine to run the executable.

%package -n cygwin64-gdb
Summary:        GNU Debugger for Cygwin 64bit toolchain
# NB: This must be left in.
Requires:       cygwin64-filesystem

%description -n cygwin64-gdb
GNU debugger which understands Cygwin 64bit PE executables and DLLs,
using a gdbserver(1) instance on a Cygwin machine to run the executable.


%prep
%setup -q -n gdb-%{version}
%patch0 -p2
%patch1 -p1
%patch2 -p1
%patch3 -p1


%build
mkdir -p build_cyg32
pushd build_cyg32
CFLAGS="$RPM_OPT_FLAGS -DLINKER_SYMBOLS_HAVE_WIN32_STDCALL_ARG_SIZES=1" \
../configure \
  --build=%_build --host=%_host \
  --target=%{cygwin32_target} \
  --verbose --disable-nls \
  --without-included-gettext \
  --disable-win32-registry \
  --disable-werror \
  --with-sysroot=%{cygwin32_sysroot} \
  --prefix=%{_prefix} --bindir=%{_bindir} \
  --includedir=%{_includedir} --libdir=%{_libdir} \
  --mandir=%{_mandir} --infodir=%{_infodir} \
  --with-gdb-datadir=%{_datadir}/gdb/%{cygwin32_target}

make all
popd

mkdir -p build_cyg64
pushd build_cyg64
CFLAGS="$RPM_OPT_FLAGS" \
../configure \
  --build=%_build --host=%_host \
  --target=%{cygwin64_target} \
  --verbose --disable-nls \
  --without-included-gettext \
  --disable-win32-registry \
  --disable-werror \
  --with-sysroot=%{cygwin64_sysroot} \
  --prefix=%{_prefix} --bindir=%{_bindir} \
  --includedir=%{_includedir} --libdir=%{_libdir} \
  --mandir=%{_mandir} --infodir=%{_infodir} \
  --with-gdb-datadir=%{_datadir}/gdb/%{cygwin64_target}

make %{?_smp_flags}
popd

%install
pushd build_cyg32
make DESTDIR=$RPM_BUILD_ROOT install
popd
pushd build_cyg64
make DESTDIR=$RPM_BUILD_ROOT install
popd

# This file conflicts with ordinary binutils.
rm -f $RPM_BUILD_ROOT%{_libdir}/libiberty.a

# These files conflict with ordinary gdb.
rm -rf $RPM_BUILD_ROOT%{_includedir}
rm -rf $RPM_BUILD_ROOT%{_infodir}
# gdbserver is not installed in this configuration
rm -f $RPM_BUILD_ROOT%{_mandir}/man1/*gdbserver*


%files -n cygwin32-gdb
%doc COPYING3 COPYING3.LIB COPYING COPYING.LIB README
%{_bindir}/%{cygwin32_target}-gdb
%{_datadir}/gdb/%{cygwin32_target}/
%{_mandir}/man1/%{cygwin32_target}-gdb.1*
%{_mandir}/man5/%{cygwin32_target}-gdbinit.5*

%files -n cygwin64-gdb
%doc COPYING3 COPYING3.LIB COPYING COPYING.LIB README
%{_bindir}/%{cygwin64_target}-gdb
%{_datadir}/gdb/%{cygwin64_target}/
%{_mandir}/man1/%{cygwin64_target}-gdb.1*
%{_mandir}/man5/%{cygwin64_target}-gdbinit.5*


%changelog
* Mon Sep 12 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 7.10.1-1
- new version

* Thu Aug 20 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 7.9.1-1
- New version with split support for Cygwin 32- and 64-bit toolchains

* Tue Sep 04 2012 Yaakov Selkowitz <yselkowitz@users.sourceforge.net> - 7.5.50-1
- Version bump to match Cygwin distro.

* Tue Nov 08 2011 Yaakov Selkowitz <yselkowitz@users.sourceforge.net> - 7.3.50.20110821-1
- Version bump to match Cygwin distro.

* Sun Jul 31 2011 Yaakov Selkowitz <yselkowitz@users.sourceforge.net> - 7.3.50.20110801-1
- Version bump to match Cygwin distro.

* Mon Jul 04 2011 Yaakov Selkowitz <yselkowitz@users.sourceforge.net> - 7.2-2
- Rebuilt for http://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Mar 21 2011 Yaakov Selkowitz <yselkowitz@users.sourceforge.net> - 7.2-1
- Initial RPM release.
